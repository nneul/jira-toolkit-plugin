package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;

import java.util.Map;

public class NullCFType extends CalculatedCFType
{
    private PermissionManager permissionManager;
    private JiraAuthenticationContext authenticationContext;
    private final IssueManager issueManager;

    public NullCFType(PermissionManager permissionManager, JiraAuthenticationContext authenticationContext, IssueManager issueManager)
    {
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
        this.issueManager = issueManager;
    }

    public String getStringFromSingularObject(Object singularObject)
    {
        return null;
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException
    {
        return null;
    }

    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        return null;
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem)
    {
        //retrieving the issue gv because the Issue Object is not fully backed by the GV
        boolean ableToEditIssue = false;
        boolean ableToComment = false;
        if (issue != null)
        {
            ableToEditIssue = issueManager.isEditable(issue) && permissionManager.hasPermission(Permissions.EDIT_ISSUE, issue, authenticationContext.getUser());
            ableToComment = permissionManager.hasPermission(Permissions.COMMENT_ISSUE, issue, authenticationContext.getUser());
        }

        Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);
        map.put("ableToEditIssue", ableToEditIssue ? Boolean.TRUE : Boolean.FALSE);
        map.put("ableToComment", ableToComment ? Boolean.TRUE : Boolean.FALSE);
        return map;
    }
}
