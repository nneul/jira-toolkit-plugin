package com.atlassian.jira.toolkit.customfield.format;

import com.atlassian.core.util.DateUtils;

import java.util.Date;

/**
 * @since v0.26
 */
public class LastCommentDateFormatter
{
    public String format(DateUtils dateUtils, Date lastComment)
    {
        long elapsedTime = System.currentTimeMillis() - lastComment.getTime();
        if (elapsedTime < 60 * DateUtils.SECOND_MILLIS)
            return "a moment ago";

        if (elapsedTime > DateUtils.DAY_MILLIS)
        {
            // If more than a day we don't want to see hours or minutes, so round it down
            elapsedTime = elapsedTime - (elapsedTime % DateUtils.DAY_MILLIS);
        }
        else if (elapsedTime > DateUtils.HOUR_MILLIS)
        {
            // More than an hour - round it down to show hours but not minutes
            elapsedTime = elapsedTime - (elapsedTime % DateUtils.HOUR_MILLIS);
        }

        // DateUtils expects a value in seconds
        return dateUtils.formatDurationPretty(elapsedTime / 1000) + " ago";
    }

    public boolean isOld(Date lastComment)
    {
        long elapsedTime = System.currentTimeMillis() - lastComment.getTime();
        return (elapsedTime > 7 * DateUtils.DAY_MILLIS);
    }

    public boolean isVeryOld(Date lastComment)
    {
        long elapsedTime = System.currentTimeMillis() - lastComment.getTime();
        return (elapsedTime > 28 * DateUtils.DAY_MILLIS);
    }
}
